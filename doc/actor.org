# -*- coding: utf-8; -*-
#+title: アクター

* アクターとは

アクターはプレイヤー・キャラクターとクリーチャーの総称です。
アクターは基本的に以下の能力値を持っています:

- 技術点
- 体力点
- 運点
- 魔法点

* それぞれの能力値の説明

** 技術点 (skill)

アクターの戦闘などがどれだけできるかの能力を表します。
装備や怪我などによって上下することがあります。

** 体力点 (stamina)

アクターの体力を表します。
怪我・病気・疲労などで減少し、食事や急速を摂ることで回復します。

** 運点 (luck)

アクターの運の良さを表します。
判定は現在の運点を目標とした下方判定です。

** 魔法点 (magic)

アクターの魔法の素質や魔法がどれだけできるかの能力を表します。
装備や怪我などによって上下することがあります。

* ソースコード

#+begin_src c
  typedef struct cfc_actor_stat {
      int16_t    skill;             //!< 技術点
      int16_t    stamina;           //!< 体力展
      int16_t    luck;              //!< 運点
      int16_t    magic;             //!< 魔法点

      int16_t    weapon_class;
      int16_t    armour_class;

      int32_t    level;

      int32_t    hit_point;
      int32_t    max_hit_point;

      int32_t    mana_point;
      int32_t    max_mana_point;

      int16_t    stomatch_degree;      //!< 満腹度
      uint32_t   experience;           //!< 経験値
      int32_t    speed;                //!< 速度
    
  }  CFCActorStat;
#+end_src
